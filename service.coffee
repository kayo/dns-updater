{get, post} = require "restler"

class Plugin
  @plugin = (@name)->
    @class = {}
    
  @register = (name)->
    @class[name] = @
  
  @create = (config)->
    throw new Error "Invalid `#{@name}` options group in the config. An object expected" unless typeof config is "object"
    {plugin} = config
    throw new Error "The required `plugin` option not found in `#{@name}` options group" unless plugin
    Class = @class[plugin]
    throw new Error "Unknown `plugin` #{plugin}. It must be one of #{(name for name of @class).join ", "}." unless Class
    new Class config

class Checker extends Plugin
  constructor: ({@attempts})->
    @attempts ?= 7

  abort: ->
    @req?.abort?()

  check_ip: (callback)->
    attempts = @attempts
    do action = =>
      @fetch_ip (error, ip)=>
        if error
          if attempts > 0
            attempts -= 1
            do action
          else
            callback new Error "Unable to fetch ip address"
        else
          callback null, ip

Checker.plugin "check"

class SimpleChecker extends Checker
  constructor: ({@services})->
  
  get_element_random = (list)->
    list[Math.round (do Math.random * (list.length - 1))]
  
  fetch_ip: (callback)->
    @req = get (get_element_random @services)
    .on "success", (data)=>
      delete @req
      if data and m = data.match /\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/
        callback null, m[0]
      else
        callback new Error "Unable to find ip address"
    .on "error", (error)=>
      delete @req
      callback new Error "Unable to check ip #{error.message}"

SimpleChecker.register "simple"

class Updater extends Plugin
  constructor: ({@attempts})->
    @attempts ?= 3

  abort: ->
    @req?.abort?()

  update_record: (ip, callback)->
    attempts = @attempts
    do action = =>
      @set_record ip, (error)->
        if error
          if attempts > 0
            attempt -= 1
            do action
          else
            callback error
        else
          callback null

Updater.plugin "update"

class DyndnsUpdater extends Updater
  constructor: ({@username, @password, @hostname, useragent})->
    throw new Error "The required `username` option not found in `update` group" unless @username
    throw new Error "The required `password` option not found in `update` group" unless @password
    throw new Error "The required `hostname` option not found in `update` group" unless @hostname
    
    @headers =
      "User-Agent": useragent or "illumium.org - nixie - 0.0.1"

  api_url = "https://members.dyndns.org/nic/update"
  
  set_record: (myip, callback)->
    wildcard = mx = backmx = "NOCHG"
    @req = get api_url, {@headers, @username, @password, query: {@hostname, myip, wildcard, mx, backmx}}
    .on "success", (data)=>
      delete @req
      if data
        if data is "good"
          callback null
        else
          callback new Error "Unable to update record (#{data})"
      else
        callback new Error "Empty response"
    .on "error", (error)=>
      delete @req
      callback new Error "Unable to get records (#{error.message})"

DyndnsUpdater.register "dyndns"

class YandexUpdater extends Updater
  constructor: ({@domain, @subdomain, @PddToken, @ttl})->
    throw new Error "The required `domain` option not found in `update` group" unless @domain
    throw new Error "The required `subdomain` option not found in `update` group" unless @subdomain
    throw new Error "The required `PddToken` option not found in `update` group" unless @PddToken
    @ttl ?= 300 # 5 minutes by default

  api_base = "https://pddimp.yandex.ru/api2/admin/dns"
  type = "A"
  
  set_record: (content, callback)->
    @req = get "#{api_base}/list",
      headers: {@PddToken}
      query: {@domain}
    .on "success", (data)=>
      delete @req
      if data?.error
        callback new Error "Unable to get records (#{error.message})"
      else if data?.records
        [record_id, ...] = (record_id for {record_id, subdomain} in data.records when subdomain is @subdomain)
        @req = (if record_id
          post "#{api_base}/edit",
            headers: {@PddToken}
            data: {@domain, @subdomain, record_id, type, content, @ttl}
        else
          post "#{api_base}/add",
             headers: {@PddToken}
             data: {@domain, @subdomain, type, content, @ttl})
        .on "success", (data)=>
          if data?.error
            callback new Error "Unable to set record (#{error})"
          else
            callback null
        .on "error", (error)=>
          callback new Error "Unable to set record (#{error.message})"
      else
        callback new Error "Unable to get records"
    .on "error", (error)=>
      delete @req
      callback new Error "Unable to get records (#{error.message})"

YandexUpdater.register "yandex"

class Service
  constructor: (config)->
    throw new Error "Invalid config" unless typeof config is "object"
    throw new Error "The `check` options group not found in the config" unless config.check
    throw new Error "The `update` options group not found in the config" unless config.update
    {@interval} = config
    @interval ?= 30000
    @checker = Checker.create config.check
    @updater = Updater.create config.update
    @run = no

  schedule: ->
    @timer = setTimeout @update, @interval
  
  update: =>
    console.log "Checking ip address..."
    @checker.check_ip (error, ip)=>
      return unless @run
      if ip
        if ip isnt @ip
          console.log "Updating record using #{ip}"
          @updater.update_record ip, (error)=>
            return unless @run
            if error
              console.error error.message
            else
              console.log "Update success."
              @ip = ip
            do @schedule
        else
          do @schedule
      else
        if error
          console.error error.message
        do @schedule

  start: ->
    @run = yes
    @ip = null
    do @update

  stop: ->
    @run = no
    clearInterval @timer
    do @checker.abort
    do @updater.abort

service = new Service require "./config.json"

process.on "SIGINT", ->
  do service.stop
  process.exit 0

do service.start
